function getCookie(name)
{
	var cookieValue = null;
	if(document.cookie && document.cookie != '')
	{
		var cookies = document.cookie.split(';');
		for(var i = 0 ; i < cookies.length; i++)
		{
			var cookie = jQuery.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) == (name + '='))
			{
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

function csrfSafeMethod(method){
	// these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(document).ready(function()
{
    $.ajaxSetup({
			beforeSend : function(xhr,settings)
			{
				if (!csrfSafeMethod(settings.type) && !this.crossDomain)
				{
					xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
				}
			}
	});
	
	var objEnviar =
	{	
		login: null,
		senha : null,
	};
	
	$("#logar").click(function(event)
	{
		objEnviar.login = $("input#login").val();
		objEnviar.senha = $("input#senha").val();
		console.log(document.URL);
		console.log(objEnviar);
		$.ajax({
			url:document.URL,
			type:'POST',
			data: objEnviar,
			success:function(json,textStatus,jqXHR)
			{
				recebido = jQuery.parseJSON(json);
				console.log(recebido);
				window.location.replace(recebido);
				//setTimeout("window.location.replace(recebido);", 500);
			},
			error:function(xhr,errmsg,err)
			{
				console.log(xhr);
				console.log(errmsg);
				console.log(err);
				console.log(xhr.responseText);
			}
		});
	});
	
	
	
	
});
