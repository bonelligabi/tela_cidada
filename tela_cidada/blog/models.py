from django.db import models

from django.db import models
from django import forms
from django.forms import ModelForm
from django.utils import timezone
from administrador.models import Funcionario

class Enquete(models.Model):
	pergunta = models.CharField(max_length=200)
	data = models.DateTimeField()
	funcionario = models.ForeignKey(Funcionario)

class Resposta(models.Model):
	enquete = models.ForeignKey(Enquete)
	resposta= models.CharField(max_length=200)

class Tag(models.Model):
	nome = models.CharField(max_length=50)	

class Postagem(models.Model):
	funcionario = models.ForeignKey(Funcionario)
	data = models.DateTimeField(default=timezone.now())
	titulo = models.CharField(max_length=200)
	texto = models.TextField()

class Postagem_Tag(models.Model):
	tag = models.ForeignKey(Tag)
	postagem = models.ForeignKey(Postagem)
