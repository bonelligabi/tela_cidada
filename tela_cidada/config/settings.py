"""
Django settings for site_ncd project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

import os
import MySQLdb

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(os.path.dirname(os.path.abspath(__file__))))

AUTH_USER_MODEL = 'administrador.Funcionario'

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

SECRET_KEY = '%q7boiwz(188r9)p_@i*l2birb*gnn+*3zrr@qp_=c4hwrppny'

DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'administrador',
    'blog',
    'base',
    'federal',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.mysql',
        'NAME': 'TELA_CIDADA',
        #'ENGINE':'django.db.backends.sqlite3',
        #'NAME': 'tela_db',
        'USER':'root',
        #'HOST': '',
        'PASSWORD':'PfNra5p4!',
        #'HOST': '172.20.82.215',
        #'PASSWORD':'abacateabacate',
        'PORT': '',
    }
}

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


MEDIA_ROOT = os.path.join(PROJECT_ROOT_PATH, 'midia')
MEDIA_URL = '/midia/'

STATIC_ROOT = os.path.join(PROJECT_ROOT_PATH, 'static')
STATIC_URL = '/static/'

ADMINISTRADOR_ROOT_PATH = os.path.join(PROJECT_ROOT_PATH,'administrador')
BASE_ROOT_PATH = os.path.join(PROJECT_ROOT_PATH,'base')
BLOG_ROOT_PATH = os.path.join(PROJECT_ROOT_PATH,'blog')
FEDERAL_ROOT_PATH = os.path.join(PROJECT_ROOT_PATH,'federal')

STATICFILES_DIRS = (
    os.path.join(ADMINISTRADOR_ROOT_PATH,'templates'),
    os.path.join(BASE_ROOT_PATH,'templates'),
    os.path.join(BLOG_ROOT_PATH,'templates'),
    os.path.join(FEDERAL_ROOT_PATH,'templates'),
)

TEMPLATE_DIRS = (
    os.path.join(ADMINISTRADOR_ROOT_PATH,'templates'),
    os.path.join(BASE_ROOT_PATH,'templates'),
    os.path.join(BLOG_ROOT_PATH,'templates'),
    os.path.join(FEDERAL_ROOT_PATH,'templates'),
)

MEDIA_DIRS = (
    os.path.join(ADMINISTRADOR_ROOT_PATH,'midia'),
    os.path.join(BASE_ROOT_PATH,'midia'),
    os.path.join(BLOG_ROOT_PATH,'midia'),
    os.path.join(FEDERAL_ROOT_PATH,'midia'),
)
