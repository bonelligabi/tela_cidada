from django.shortcuts import render

def home(request):
	return render(request, 'xhtml/home.xhtml', {})

def guia(request):
	return render(request, 'xhtml/guia.xhtml', {})
