var slideIndex = 1;
var nImagens = 3;

function showSlides(n) 
{
	var i;
	var fundoSlideShow = $('#FundoSlideShow');
	fundoSlideShow.css({'background':'url(/midia/img' + n + '.jpg) no-repeat'});
	fundoSlideShow.css({'background-size': '100% 100%'});
}

function plusSlides(n) 
{
	slideIndex += n;
	
	if(slideIndex > 3)
		slideIndex = 1;
		
	if(slideIndex < 1)
		slideIndex = 3;
		
	showSlides(slideIndex);
	console.log(slideIndex);
}


$(document).ready(function()
{		
	showSlides(1);
	var divFundoMenu = $('#FundoMenu');  
	var divMenu = $('#Menu'); 
	
	$('#facebook').attr("src","/midia/icone_facebook.png");
	$('#instagram').attr("src","/midia/icone_instagram.png");
	$('#youtube').attr("src","/midia/icone_youtube.png");
	$('#logoTelaCidada').attr("src","/midia/logo.png");	
	
	divFundoMenu.addClass("ExtensaoFundoMenu");  
	$(window).scroll(function () 
	{ 
		if ($(this).scrollTop() > 50)
		{
			$('#FundoMenu').css({"height":"69px"}); 
			$('#FundoRedeSociais').css({"top":"0px"});
			$('#menu ul li').css({"top":"26px"});
			$('#LAI').text("Lei de Acesso à Informação");
			$('#menu ul li').css({"left": "160px"});
			$('#FundoMenu').css({"background": "rgba(0, 0, 0, .7)"});
			$('#logoTelaCidada').hide();
		}
		else 
		{
			$('#FundoMenu').css({"height":"110px"});
			$('#FundoRedeSociais').css({"top":"20px"});
			$('#menu ul li').css({"top":"60px"});
			$('#LAI').text("LAI");
			$('#menu ul li').css({"left": "355px"});
			$('#FundoMenu').css({"background": "black"});
			$('#logoTelaCidada').show();
		}
	});  
});
