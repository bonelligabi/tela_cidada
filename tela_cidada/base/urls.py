#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('base.views',
	# Examples:

	# url(r'^$', 'NCDSIS.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),
	url(r'^$', 'home'),
	url(r'^guia/', 'guia'),
	
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
